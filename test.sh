#!/bin/bash

create() {
  curl -sS -X POST http://localhost:8000/create -H 'Content-Type: application/json' -d '{"Id":"'$1'","Balance":"'$2'"}'
  echo ""
}

send() {
  a=$(curl -sS -X POST http://localhost:8000/send -H 'Content-Type: application/json' -d '{"From":"'$1'","To":"'$2'","Amount":"'$3'"}')
  echo $a >&2 # Print to error so we can get stdout
  id=${a/\{\"Id\":\"/}
  id=${id/\"\}/}
  echo $id
}

confirm() {
  curl -sS -X POST http://localhost:8000/confirm -H 'Content-Type: application/json' -d '{"Id":"'$1'"}'
  echo ""
}

print() {
  curl -sS http://localhost:8000/print
  echo ""
}

debug() {
  curl -sS http://localhost:8000/debug
  echo ""
}

# Test Transactions
echo "------------- Creating Accounts -------------"
create Greg1 50.0
create Greg2 0.0
create foo 90
create bar 10004.32

echo -e "\n\n\n"
echo "------------- Current Ledger State -------------"
print

echo -e "\n\n\n"
echo "------------- Doing Transactions (Finish With 10/20/30/40) -------------"
id=$(send Greg1 Greg2 46.95)
confirm $id
id=$(send Greg1 foo 3.05)
confirm $id
id=$(send foo Greg1 10)
confirm $id
id=$(send Greg2 foo 26.45)
confirm $id
id=$(send Greg2 foo 0.5)
confirm $id
id=$(send bar Greg2 0.32)
confirm $id
id_1=$(send Greg2 bar 0.32)
id_2=$(send bar foo 9974.32)
confirm $id_1 # Testing delayed confirmation
confirm $id_2
id=$(send foo Greg1 9916.89)
id=$(send foo Greg1 103.43)
id=$(send foo bar 24)

echo -e "\n\n\n"
echo "------------- Current Ledger State -------------"
print
# Uncomment debug below to see how structures are stored internally
# debug

echo -e "\n\n\n"

echo "------------- Failure Tests -------------"
echo "Trying to reply transaction"
confirm $id # Succeeds
confirm $id # Fails
echo ""

echo "Trying Bad JSON"
curl -sS -X POST http://localhost:8000/confirm -H 'Content-Type: application/json' -d 'greg"'243'"}'
echo ""
echo ""

echo "Trying Bad Id"
confirm 234
echo ""

echo "Trying to Confirm with missing fields"
curl -sS -X POST http://localhost:8000/confirm -H 'Content-Type: application/json' -d '{"greg":"'243'"}'
echo ""
echo ""

echo "Trying Transaction with missing fields" 
curl -sS -X POST http://localhost:8000/send -H 'Content-Type: application/json' -d '{"rom":"Greg1","To":"Greg2","Amount":"46.95"}'
echo ""
echo ""

echo "Trying to Create account with missing fields"
curl -sS -X POST http://localhost:8000/create -H 'Content-Type: application/json' -d '{"Id":"Greg2","Boolance":"0.0"}'
echo ""
echo ""

echo "Trying to create duplicate account"
create Greg2 0.0
echo ""

echo "Trying to create balance with leading zeros"
create Greg3 000.0
echo ""

echo "Trying to create negative balance"
create Greg3 -4
echo ""

echo "Trying to create HUGE balance"
create Greg3 99999999999999999999999
echo ""

echo "Trying to create tiny balance"
create Greg3 0.000000001
echo ""

echo "Trying to create invalid balance"
create Greg3 0.0.000000001
echo ""

echo "Trying to create with invalid account id"
create '$(@#(*$' 0.0
echo ""

echo "Trying to send too much $$"
send Greg1 Greg2 10000 >/dev/null
echo ""

echo "Trying to send to self"
send Greg1 Greg1 0.01 >/dev/null
echo ""

echo "Trying to send from non-existent account"
send IDontExist Greg1 0.01 >/dev/null
echo ""

echo "Trying to send to non-existent account"
send Greg1 IDontExist 0.01 >/dev/null
echo ""

echo "Trying to send from bad account name"
send '$$#441' Greg1 0.01 >/dev/null
echo ""

echo "Trying to send bad amount"
send Greg1 Greg2 0.0.1 >/dev/null
echo ""
