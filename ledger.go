/*
  Description: A Simple Ledger with a REST API.
  Author: Greg Hysen
  Date: 12/17/17
*/
package main

import (
	"fmt"
	"strings"
	"regexp"
	"os/exec"
	"math"
	"strconv"
	"sort"
	"time"
)

type errorString struct {
    s string
}

func (e *errorString) Error() string {
    return e.s
}

// Uses command line tool <uuidgen> to generate a unique confirmation id
func GenerateConfirmationId() (string, error) {
	out, err := exec.Command("uuidgen").Output()
    if err != nil {
        return "", &errorString{fmt.Sprintf("Oops! Something went wrong on our end. Please try your request later.")}
    }
    return string(out[:len(out)-1]), nil
}

// Max value a uint64 can hold
const MaxUint = ^uint64(0)

// A currency is defined by an ID and the precision an asset may use
// For example, USD has precision of 2 -- 0-99 cents.
// Another currency, like bitcoin, would use a higher precision.
type Currency struct {
	Id string
	Precision uint32
}

// Enumerate available currencies
var (
	USD = Currency{Id: "USD", Precision: 2}
)

func NewCurrency(id string) (currency *Currency, err error) {
	switch id {
	case USD.Id:
		return &USD, nil
	}

	return nil, &errorString{fmt.Sprintf("Invalid currency '%s'", id)}
}

// An asset is some amount of a specific currency
// We use a uint64 for all currencies to avoid floating point complications
// The mantissa (before decimal) and characteristic (after decimal)
// are combined into this single integral value by multiplying
// the mantissa by 10^Currency.Precision then adding the characteristic
type Asset struct {
	Value uint64
	Currency *Currency
}

// Add two assets together
func (lhs *Asset) Add(rhs *Asset) error {
	if lhs.Value > (MaxUint - rhs.Value) {
		return &errorString{fmt.Sprintf("Unsigned Integer Overflow")}
	}
	lhs.Value += rhs.Value
	return nil
}

// Subtract assets from each other
func (lhs *Asset) Sub(rhs *Asset) error {
	if rhs.Value > lhs.Value {
		return &errorString{fmt.Sprintf("Unsigned Integer Overflow")}
	}
	lhs.Value -= rhs.Value
	return nil
}

// Convert our asset to a pretty string, ex "10.00 USD"
func (asset *Asset) ToStr() string {
	var mantissa uint64 = asset.Value / uint64(math.Pow10(int(asset.Currency.Precision)))
	var characteristic uint64 = asset.Value % uint64(math.Pow10(int(asset.Currency.Precision)))
	printf_style := "%d.%0" + fmt.Sprintf("%d", asset.Currency.Precision) + "d %s"
	return fmt.Sprintf(printf_style, mantissa, characteristic, asset.Currency.Id)
}

// Asset Constructor
func NewAsset(currency *Currency) *Asset {
	return &Asset{Value: 0, Currency: currency}
}

// Asset Generator from string inputs
func CreateAsset(value string, currency_id string) (*Asset, error) {
	// Valid Examples: 	 1245 OR 0.943  OR 104.044
	// Invalid Eaxmples: 0123 OR 0123.5 OR 104.
	r, _ := regexp.Compile("(^[0-9]+$)|(^[1-9]+[0-9]*$)|(^0\\.[0-9]+$)|(^[1-9][0-9]*\\.[0-9]+$)")
	if ! r.MatchString(value) {
		return nil, &errorString{fmt.Sprintf("Invalid asset value '%s'", value)}
	}

	// Split value string into array [mantissa, characteristic]
	value_split := strings.Split(value, ".")
	if len(value_split) == 1 {
		value_split = append(value_split, "0")
	}

	// Parse currency
	currency, err := NewCurrency(currency_id)
	if err != nil {
		return nil, err
	}

	// Parse mantissa (before the decimal)
	mantissa,err := strconv.ParseUint(value_split[0], 10, 64);
	if err != nil {
		return nil, &errorString{fmt.Sprintf("Invalid asset value '%s'. Bad mantissa.", value)}
	}

	// Parse characteristic (after the decimal)
	characteristic,err := strconv.ParseUint(value_split[1], 10, 64)
	if len(value_split) == 2 {
		if err != nil || len(value_split[1]) > int(currency.Precision) {
			return nil, &errorString{fmt.Sprintf("Invalid asset value '%s'. Bad characteristic.", value)}
		}
	}

	// Todo: Check these please
	if mantissa > (MaxUint / uint64(math.Pow10(int(currency.Precision)))) {
		return nil, &errorString{fmt.Sprintf("Invalid asset value '%s'. Value too high.", value)}
	}
	if characteristic >= (MaxUint / uint64(math.Pow10(int(currency.Precision)))) {
		return nil, &errorString{fmt.Sprintf("Invalid asset value '%s'. Too precise.", value)}
	}

	asset := &Asset{Value: mantissa * uint64(math.Pow10(int(currency.Precision))) + characteristic * uint64(math.Pow10(int(currency.Precision) - len(value_split[1]))), Currency: currency}
	return asset, nil
}

// An account contains an Id and a Balance
// Example: {"Id": "GregAccount", "Balance": "50"}
type Account struct {
	Id string
	Balance Asset
}

// Account Constructor
func NewAccount(id string, balance *Asset, user_account bool) (*Account,error) {
	var account Account

	// Set <account>.<id> after sanity check
	// Valid account ids can only be alphanumeric
	r, _ := regexp.Compile("[[:alnum:]]+")
	if user_account && !r.MatchString(id) {
		return nil,&errorString{fmt.Sprintf("Invalid account id: '%s'. Account id must be alpha-numeric with at least one character", id)}
	}
	account.Id = id
	account.Balance = *NewAsset(&USD)

	// Update balance on account
	if err := account.Balance.Add(balance); err != nil {
		return nil,&errorString{fmt.Sprintf("Balance limit reached")}
	}

	// Return account
	return &account,nil
}

// Deposit into account
func (account *Account) Deposit(amount *Asset) error {
	if err := account.Balance.Add(amount); err != nil {
		return &errorString{fmt.Sprintf("Balance limit reached")}
	}
	return nil
}

// Withdraw from account
func (account *Account) Withdraw(amount *Asset) error {
	if err := account.Balance.Sub(amount); err != nil {
		return &errorString{fmt.Sprintf("Insufficient funds")}
	}
	return nil
}

// Encapsulates a transaction
type Transaction struct {
	FromAccountId string 	// Account transferring funds from
	ToAccountId string 		// Account transferring funds to
	Amount Asset 			// Amount to transfer
	StartTime time.Time 	// When the transfer was started
	FinishTime time.Time 	// When the transfer completed
}

// Transaction constructor
func NewTransaction(from_account_id, to_account_id, amount string) (*Transaction, error) {
	var transaction Transaction
	transaction.FromAccountId = from_account_id
	transaction.ToAccountId = to_account_id

	// Verify from/to are not same account
	if from_account_id == to_account_id {
		return nil, &errorString{fmt.Sprintf("From and To accounts must be different")}
	}

	// Parse Amount into an Asset
	ta, err := CreateAsset(amount, USD.Id)
	if err != nil {
		return nil, err
	}
	transaction.Amount = *ta

	// Successfully created transaction. Return it.
	return &transaction, nil
}

// Encapsulates and Exhange
type Ledger struct {
	UserAccounts map[string]*Account 				// Accounts belonging to users
	Reserve *Account 								// Special reserve account for holding funds
	PendingTransactions map[string]*Transaction 	// List of pending transactions
	FinishedTransactions map[string]*Transaction 	// History of finished transactions
}

// Ledger constructor
func NewLedger() (*Ledger, error) {
	var ledger Ledger
	ledger.UserAccounts = make(map[string]*Account)
	reserve, err := NewAccount("__reserve__", NewAsset(&USD), false)
	if err != nil {
		return nil, err
	}
	ledger.Reserve = reserve
	ledger.PendingTransactions = make(map[string]*Transaction)
	ledger.FinishedTransactions = make(map[string]*Transaction)
	return &ledger, nil
}

// Add user account to the ledger
func (ledger *Ledger) AddUserAccount(account *Account) error {
	_, exists := ledger.UserAccounts[account.Id]
	if exists {
		return &errorString{fmt.Sprintf("Account '%s' already exists", account.Id)}
	}
	ledger.UserAccounts[account.Id] = account
	return nil
}

// Start a transaction on the ledger
func (ledger *Ledger) StartTransaction(transaction *Transaction) (confirmation_id string, err error) {
	// Sanity checks that accounts exist	
	from_account, exists := ledger.UserAccounts[transaction.FromAccountId]
	if !exists {
		return "", &errorString{fmt.Sprintf("From Account '%s' does not exist", transaction.FromAccountId)}
	}
	
	if _, exists := ledger.UserAccounts[transaction.ToAccountId]; !exists {
		return "", &errorString{fmt.Sprintf("To Account '%s' does not exist", transaction.ToAccountId)}
	}

	// Withdraw funds from <from_account>
	if err := from_account.Withdraw(&transaction.Amount); err != nil {
		return "", err
	}

	// Deposit funds into <reserve>
	if err := ledger.Reserve.Deposit(&transaction.Amount); err != nil {
		return "", err
	}

	// Add timestamp
	transaction.StartTime = time.Now()

	// Generate a confirmation ID
	confirmation_id, err = GenerateConfirmationId()
	if err != nil {
		return "", err
	}
	ledger.PendingTransactions[confirmation_id] = transaction
	return confirmation_id, nil
}

// Finish a transaction on the ledger
func (ledger *Ledger) FinishTransaction(confirmation_id string) error {
	// Sanity checks that accounts exist
	transaction, tx_exists := ledger.PendingTransactions[confirmation_id]
	if !tx_exists {
		return &errorString{fmt.Sprintf("Confirmation '%s' does not exist", confirmation_id)}
	}	
	if _, exists := ledger.UserAccounts[transaction.FromAccountId]; !exists {
		return &errorString{fmt.Sprintf("From Account '%s' does not exist", transaction.FromAccountId)}
	}
	to_account, acc_exists := ledger.UserAccounts[transaction.ToAccountId]
	if !acc_exists {
		return &errorString{fmt.Sprintf("To Account '%s' does not exist", transaction.ToAccountId)}
	}

	// Withdraw transaction amount from <reserve>
	if err := ledger.Reserve.Withdraw(&transaction.Amount); err != nil {
		return err
	}

	// Deposit transaction amount into <to_account>
	if err := to_account.Deposit(&transaction.Amount); err != nil {
		return err
	}

	// Add timestamp
	transaction.FinishTime = time.Now()
	
	// This transaction has been completed. Delete it from list of pending transactions.
	ledger.FinishedTransactions[confirmation_id] = transaction
	delete(ledger.PendingTransactions, confirmation_id)
	return nil
}

// Print the ledger
func (ledger *Ledger) Print() (output string) {
	output = fmt.Sprintln("-- User Accounts --")
	var keys []string
    for k := range ledger.UserAccounts {
        keys = append(keys, k)
    }
    sort.Strings(keys)
	for _,key := range keys {
		output += fmt.Sprintln(ledger.UserAccounts[key].Id, ":", ledger.UserAccounts[key].Balance.ToStr())
	}
	output += fmt.Sprintln("")

	output += fmt.Sprintln("-- Reserve --")
	output += fmt.Sprintln(ledger.Reserve.Balance.ToStr())
	output += fmt.Sprintln("")

	output += fmt.Sprintln("-- Pending Transactions --")
	for id,tx := range ledger.PendingTransactions {
		output += fmt.Sprintln(id, ":", tx.FromAccountId, "->", tx.ToAccountId, "(", tx.Amount.ToStr(), ") [", tx.StartTime, "]")
	}
	output += fmt.Sprintln("")

	output += fmt.Sprintln("-- Completed Transactions --")
	for id,tx := range ledger.FinishedTransactions {
		output += fmt.Sprintln(id, ":", tx.FromAccountId, "->", tx.ToAccountId, "(", tx.Amount.ToStr(), ") [", tx.StartTime, "]")
	}
	output += fmt.Sprintln("")

	return output
}
