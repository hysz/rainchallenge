/*
  Description: A Simple Ledger with a REST API.
  Author: Greg Hysen
  Date: 12/17/17
*/
package main

import (
	"net/http"
	"github.com/labstack/echo"
	"encoding/json"
)

type CustomEchoContext struct {
	// Default echo context
	echo.Context

	// The Ledger
	ledger *Ledger
}

type ErrorResponse struct {
	Error string
}

type CreateAccountRequest struct {
	Id string
	Balance string
}

type CreateAccountResponse struct {
	Id string
}

func HandleREST_CreateAccount(c echo.Context) (err error) {
	// Use our custom context
	cc := c.(*CustomEchoContext)
	ledger := cc.ledger

	// Parse JSON
	req := new(CreateAccountRequest)
	if err = c.Bind(req); err != nil {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{"Malformed Request"})
	}
	if len(req.Id) == 0 {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{"Malformed Request. 'Id' field is missing or empty."})
	}
	if len(req.Balance) == 0 {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{"Malformed Request. 'Id' field is missing or empty."})
	}

	// Create Account
	asset,ca_err := CreateAsset(req.Balance, USD.Id)
	if ca_err != nil {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{ca_err.Error()})
	}
	account,na_err := NewAccount(req.Id, asset, true)
	if na_err != nil {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{na_err.Error()})
	}

	if err := ledger.AddUserAccount(account); err != nil {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{err.Error()})
	}

	// Return response
	res := new(CreateAccountResponse)
	res.Id = req.Id
	return c.JSON(http.StatusOK, res)
}

type StartTransactionRequest struct {
	From string
	To string
	Amount string
}

type StartTransactionResponse struct {
	Id string
}

func HandleREST_StartTransaction(c echo.Context) (err error) {
	// Use our custom context
	cc := c.(*CustomEchoContext)
	ledger := cc.ledger

	// Parse JSON
	req := new(StartTransactionRequest)
	if err = c.Bind(req); err != nil {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{"Malformed Request"})
	}
	if len(req.From) == 0 {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{"Malformed Request. 'From' field is missing or empty."})
	}
	if len(req.To) == 0 {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{"Malformed Request. 'To' field is missing or empty."})
	}
	if len(req.Amount) == 0 {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{"Malformed Request. 'Amount' field is missing or empty."})
	}

	// Create transaction
	transaction, nt_err := NewTransaction(req.From, req.To, req.Amount)
	if nt_err != nil {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{nt_err.Error()})
	}

	// Start transaction
	confirmation_id, st_err := ledger.StartTransaction(transaction)
	if st_err != nil {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{st_err.Error()})
	}

	// Return confirmation ID
	res := new(StartTransactionResponse)
	res.Id = confirmation_id
	return c.JSON(http.StatusOK, res)
}

type FinishTransactionRequest struct {
	Id string
}

type FinishTransactionResponse struct {
	Id string
}

func HandleREST_ConfirmTransaction(c echo.Context) (err error) {
	// Use our custom context
	cc := c.(*CustomEchoContext)
	ledger := cc.ledger

	// Parse JSON
	req := new(FinishTransactionRequest)
	if err = c.Bind(req); err != nil {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{"Malformed Request"})
	}
	if len(req.Id) == 0 {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{"Malformed Request. 'Id' field is missing or empty."})
	}

	// Confirm transaction
	if err := ledger.FinishTransaction(req.Id); err != nil {
		return c.JSON(http.StatusBadRequest, &ErrorResponse{err.Error()})
	}

	// Return response
	res := new(FinishTransactionResponse)
	res.Id = req.Id
	return c.JSON(http.StatusOK, res)
}

func HandleREST_PrintLedger(c echo.Context) error {
	// Use our custom context
	cc := c.(*CustomEchoContext)
	ledger := cc.ledger
	str := ledger.Print()

	return c.String(http.StatusOK, str)
}

func HandleREST_DebugLedger(c echo.Context) error {
	// Use our custom context
	cc := c.(*CustomEchoContext)
	ledger := cc.ledger

	b, err := json.MarshalIndent(ledger, "", "  ")
	if err != nil {
		return err
	}

	return c.String(http.StatusOK, string(b))
}
