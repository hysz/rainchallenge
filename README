-- About --
  Description: A Simple Ledger with a REST API.
  Author: Greg Hysen
  Date: 12/17/17

-- Deliverables --
  server.go     - Configures and runs the server
  api.go        - The REST API
  ledger.go   - Logic for accounts/transactions/etc 

-- Dependencies --
Echo Framework: go get -u github.com/labstack/echo/
/usr/bin/uuidgen: [should exist on Mac/Linux]

-- Compile & Run --
  go run server.go ledger.go api.go 

-- APIs --
  Description: Creates a new user account 
  Path: /create
  Params: 
         Id      - Unique identifier for account. Ex "Greg" 
         Balance - Initial balance of account. Ex "40.00"
  Returns:
        Id       -  Id of newly created user (equal to input Id)
  Examples:
    curl -sS -X POST http://localhost:8000/create -H 'Content-Type: application/json' -d '{"Id":"G1","Balance":"40.00"}'
    curl -sS -X POST http://localhost:8000/create -H 'Content-Type: application/json' -d '{"Id":"G2","Balance":"0.0"}'


  Description: Start transaction between two accounts
  Path: /send
  Params: 
         From   - Account to send from
         To     - Account to send to
         Amount - Amount to send
  Returns:
         Id     - Confirmation Id of transaction
  Example:
    curl -sS -X POST http://localhost:8000/send -H 'Content-Type: application/json' -d '{"From":"G1","To":"G2","Amount":"40.00"}'


  Description: Completes transaction between two accounts 
  Path: /confirm
  Params: 
         Id     - Confirmation Id of transaction
  Returns:
         Id     - Confirmation Id of transaction
  Example:
    curl -sS -X POST http://localhost:8000/confirm -H 'Content-Type: application/json' -d '{"Id":"some-uuid"}'


  Description: Prints all accounts and transactions
  Path: /print
  Params: [none] 
  Returns: [none]
  Example:
    curl -sS http://localhost:8000/print


  Description: Prints internal data structures for all accounts and transactions
  Path: /print
  Params: [none] 
  Returns: [none]
  Example:
    curl -sS http://localhost:8000/debug

-- Run Tests --
  1. go run server.go ledger.go api.go 
  2. ./test.sh
 
  See test.sh for more information on the tests
