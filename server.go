/*
  Description: A Simple Ledger with a REST API.
  Author: Greg Hysen
  Date: 12/17/17
*/
package main

import (
	"github.com/labstack/echo"
)

func main() {
	// Setup Echo Framework
	e := echo.New()

	// Setup Ledger
	ledger,err := NewLedger()
	if err != nil {
		e.Logger.Fatal(err)
	}

	// Setup Middleware
	e.Use(func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := &CustomEchoContext{c, ledger}
			return h(cc)
		}
	})
	
	// Setup REST Handlers
	e.POST("/create", HandleREST_CreateAccount)
	e.POST("/send", HandleREST_StartTransaction)
	e.POST("/confirm", HandleREST_ConfirmTransaction)
	e.GET("/print", HandleREST_PrintLedger)
	e.GET("/debug", HandleREST_DebugLedger)

	// Start Server
	e.Logger.Fatal(e.Start(":8000"))
}
